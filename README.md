# My parameters

Here is InnoCar Specs:
Budet car price per minute = 16
Luxury car price per minute = 42
Fixed price per km = 15
Allowed deviations in % = 6
Inno discount in % = 6

# BVA Table

| Parameter        | Equivalence Class              |
| ---------------- | ------------------------------ |
| type             | luxury; budget; absurd; \_     |
| plan             | minute; fixed*price; absurd; * |
| distance         | <= 0; > 0; \_                  |
| planned_distance | <= 0; > 0; \_                  |
| time             | <= 0; > 0; \_                  |
| planned_time     | <= 0; > 0; \_                  |
| inno_discount    | yes; no; absurd; \_            |

# Decision table

|     | `type` | `plan`      | `distance` | `planned_distance` | `time` | `planned_time` | `inno_discount` | `Result` |
| --- | ------ | ----------- | ---------- | ------------------ | ------ | -------------- | --------------- | -------- |
| T1  | absurd | \*          | \*         | \*                 | \*     | \*             | \*              | Invalid  |
| T2  | \*     | absurd      | \*         | \*                 | \*     | \*             | \*              | Invalid  |
| T3  | \*     | \*          | <= 0       | \*                 | \*     | \*             | \*              | Invalid  |
| T4  | \*     | \*          | \*         | <= 0               | \*     | \*             | \*              | Invalid  |
| T5  | \*     | \*          | \*         | \*                 | <= 0   | \*             | \*              | Invalid  |
| T6  | \*     | \*          | \*         | \*                 | \*     | <= 0           | \*              | Invalid  |
| T7  | \*     | \*          | \*         | \*                 | \*     | \*             | absurd          | Invalid  |
| T8  | \_     | \*          | \*         | \*                 | \*     | \*             | \*              | Invalid  |
| T9  | \*     | \_          | \*         | \*                 | \*     | \*             | \*              | Invalid  |
| T10 | \*     | \*          | \*         | \*                 | \*     | \*             | \_              | Invalid  |
| T11 | luxury | minute      | > 0        | > 0                | > 0    | > 0            | yes             | Valid    |
| T12 | luxury | minute      | > 0        | > 0                | > 0    | > 0            | no              | Valid    |
| T13 | luxury | minute      | > 0        | > 0                | \_     | > 0            | \*              | Invalid  |
| T14 | luxury | minute      | \_         | \_                 | > 0    | > 0            | yes             | Valid    |
| T15 | luxury | minute      | \_         | \_                 | > 0    | > 0            | no              | Valid    |
| T16 | luxury | fixed_price | > 0        | > 0                | > 0    | > 0            | yes             | Invalid  |
| T17 | luxury | fixed_price | > 0        | > 0                | > 0    | > 0            | no              | Invalid  |
| T18 | budget | minute      | > 0        | > 0                | > 0    | > 0            | yes             | Valid    |
| T19 | budget | minute      | > 0        | > 0                | > 0    | > 0            | no              | Valid    |
| T20 | budget | minute      | \_         | \_                 | > 0    | > 0            | yes             | Valid    |
| T21 | budget | minute      | \_         | \_                 | > 0    | > 0            | no              | Valid    |
| T22 | budget | minute      | > 0        | > 0                | \_     | \_             | \*              | Invalid  |
| T23 | budget | fixed_price | > 0        | > 0                | > 0    | > 0            | yes             | Valid    |
| T24 | budget | fixed_price | > 0        | > 0                | > 0    | > 0            | no              | Valid    |
| T25 | budget | fixed_price | > 0        | > 0                | \_     | \_             | no              | Invalid  |
| T26 | budget | fixed_price | > 0        | > 0                | \_     | \_             | yes             | Invalid  |
| T27 | budget | fixed_price | \_         | \_                 | > 0    | > 0            | \*              | Invalid  |

| # Test case | Link                                                                                                 | Expected result | Actual result                | Passed |
| ----------- | ---------------------------------------------------------------------------------------------------- | --------------- | ---------------------------- | ------ |
| T1          | type=absurd plan=minute distance=1 planned_distance=1 time=1 planned_time=1 inno_discount=yes        | Invalid Request | Invalid Request              | ✔️     |
| T2          | type=budget plan=absurd distance=1 planned_distance=1 time=1 planned_time=1 inno_discount=yes        | Invalid Request | Invalid Request              | ✔️     |
| T3          | type=budget plan=minute distance=-1 planned_distance=1 time=1 planned_time=1 inno_discount=yes       | Invalid Request | Invalid Request              | ✔️     |
| T4          | type=budget plan=minute distance=1 planned_distance=-1 time=1 planned_time=1 inno_discount=yes       | Invalid Request | Invalid Request              | ✔️     |
| T5          | type=budget plan=minute distance=1 planned_distance=1 time=-1 planned_time=1 inno_discount=yes       | Invalid Request | Invalid Request              | ✔️     |
| T6          | type=budget plan=minute distance=1 planned_distance=1 time=1 planned_time=-1 inno_discount=yes       | Invalid Request | Invalid Request              | ✔️     |
| T7          | type=budget plan=minute distance=1 planned_distance=1 time=1 planned_time=1 inno_discount=absurd     | Invalid Request | {"price":20.8}               | `FAIL` |
| T8          | plan=minute distance=1 planned_distance=1 time=1 planned_time=1 inno_discount=yes                    | Invalid Request | Invalid Request              | ✔️     |
| T9          | type=budget distance=1 planned_distance=1 time=1 planned_time=1 inno_discount=yes                    | Invalid Request | Invalid Request              | ✔️     |
| T10         | type=budget plan=minute distance=1 planned_distance=1 time=1 planned_time=1                          | Invalid Request | {"price":20.8}               | `FAIL` |
| T11         | type=luxury plan=minute distance=1 planned_distance=1 time=20 planned_time=20 inno_discount=yes      | {"price": 789.6}  | {"price":789.5999999999999}               | ✔️      |
| T12         | type=luxury plan=minute distance=1 planned_distance=1 time=20 planned_time=20 inno_discount=no       | {"price":840}  | {"price":840}                | ✔️      |
| T13         | type=luxury plan=minute distance=1 planned_distance=1 planned_time=20 inno_discount=yes              | Invalid Request | {"price":null}               | `FAIL` |
| T14         | type=luxury plan=minute time=20 planned_time=20 inno_discount=yes                                    | {"price":789,6}  | {"price":789.5999999999999}  | ✔️      |
| T15         | type=luxury plan=minute time=20 planned_time=20 inno_discount=no                                     | {"price":840}  | {"price": 840}               | ✔️      |
| T16         | type=luxury plan=fixed_price distance=1 planned_distance=1 time=20 planned_time=20 inno_discount=yes | Invalid Request | {"price":11.75}              | `FAIL` |
| T17         | type=luxury plan=fixed_price distance=1 planned_distance=1 time=20 planned_time=20 inno_discount=no  | Invalid Request | {"price":12.5}               | `FAIL` |
| T25         | type=budget plan=fixed_price distance=5 planned_distance=5 inno_discount=no                          | Invalid Request | {"price":null}               | `FAIL` |
| T26         | type=budget plan=fixed_price distance=5 planned_distance=5 inno_discount=yes                         | Invalid Request | {"price":null}               | `FAIL` |
| T27         | type=budget plan=fixed_price time=10 planned_time=10 inno_discount=yes                               | Invalid Request | {"price":156.66666666666666} | `FAIL` |

# coverage

I tested 20/27 of all possible outcomes. so the coverage is 0.74%
